using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class Menu : MonoBehaviourPunCallbacks
{   

    [Header("Go")]
    public GameObject LoginGo;
    public GameObject PartidasGo;

    [Header("Player Name")]
    public InputField PlayerNameInput;
    string PlayerNameTemp;
    public InputField NomeSala;
    public GameObject myPlayer;

    [Header("room")]
    public InputField RoomName;





    void Start()
    {
        //PhotonNetwork.ConnectUsingSettings();
        PlayerNameTemp = "Player" + Random.Range(1000, 10000);
        PlayerNameInput.text = PlayerNameTemp;

        RoomName.text = "Sala" + Random.Range(1000, 10000); 

        LoginGo.gameObject.SetActive(true);
        PartidasGo.gameObject.SetActive(false);

    }

    public void login()
    {
        
        if (PlayerNameInput.text != "")
        {
            PhotonNetwork.NickName = PlayerNameInput.text;
        }
        else
        {
            PhotonNetwork.NickName = PlayerNameTemp;
        }

        Debug.Log(PhotonNetwork.NickName);

        PhotonNetwork.ConnectUsingSettings();

        LoginGo.gameObject.SetActive(false);
        PartidasGo.gameObject.SetActive(true);
    }

    public void BuscarPartida()
    {
        PhotonNetwork.JoinLobby();
    }

    public void CriarSala()
    {
        string RoomNameTemp = RoomName.text;
        RoomOptions roomOptions = new RoomOptions() { MaxPlayers = 4 };
        PhotonNetwork.JoinOrCreateRoom(RoomNameTemp, roomOptions, TypedLobby.Default);
    }







    //###############PunCallbacks#################
    public override void OnConnected()
    {
        Debug.Log("Onconnected");
    }



    public override void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby");
        PhotonNetwork.JoinRandomRoom();

    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        string roomtemp = "room" + Random.Range(1000, 10000);
        PhotonNetwork.CreateRoom(roomtemp);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");

        LoginGo.gameObject.SetActive(false);
        PartidasGo.gameObject.SetActive(false);

        //Instantiate(myPlayer, myPlayer.transform.position,myPlayer.transform.rotation);
        PhotonNetwork.Instantiate(myPlayer.name, myPlayer.transform.position, myPlayer.transform.rotation,0);
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster");
        Debug.Log("Server:" + PhotonNetwork.CloudRegion + "Ping: " + PhotonNetwork.GetPing() );
        PhotonNetwork.CreateRoom("NomeSala");

    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Onconnected: " + cause);

    }

}
