using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BulletController : MonoBehaviour
{
    public float bulletspeed = 500f;
    Rigidbody2D rigidbody2D;
    public float bulletDamage = 10f;
    public float bulletTimeLife = 5f;
    float bulletTimeCount;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        rigidbody2D.AddForce(transform.up * bulletspeed, ForceMode2D.Force);
    }

    // Update is called once per frame
    void Update()
    {
        if(bulletTimeCount >= bulletTimeLife)
        {
            Destroy(this.gameObject);
        }

        bulletTimeCount -= Time.deltaTime;
    }

    void bulletDestroy()
    {
        Destroy(this.gameObject);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && collision.GetComponent<PlayerController>() && collision.GetComponent<PhotonView>().IsMine)
        {
            Debug.Log("Player id:" + collision.GetComponent<PhotonView>().Owner.ActorNumber);
            collision.GetComponent<PlayerController>().TakeDamage(bulletDamage);
            this.GetComponent<PhotonView>().RPC("bulletDestroy", RpcTarget.AllViaServer);
        }

        Destroy(this.gameObject);

    }



}
