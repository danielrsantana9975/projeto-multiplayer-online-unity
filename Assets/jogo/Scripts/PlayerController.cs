using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float playerSpeed = 5f;
    Rigidbody2D rigidbody2D;
    PhotonView photonView;

    Vector2 playerRB;
    Vector3 playerPosition;
    Quaternion playerRotation;

    [Header("Health")]
    public float playerHealthMax = 100f;
    float playerHealtCurrent;
    public Image playerHealthFill;

    [Header("Bullet")]
    public GameObject bullet;
    public GameObject bulletPhotonView;
    public GameObject spawnBullet;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        photonView = GetComponent<PhotonView>();
        HealthManager(playerHealthMax);
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            PlayerMove();
            PlayerTurn();
            Shooting();
        }
       
    }

    void Shooting()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //photonView.RPC("Shoot", RpcTarget.All);
            PhotonNetwork.Instantiate(bullet.name, spawnBullet.transform.position, spawnBullet.transform.rotation, 0);

        }
        /*if (Input.GetMouseButtonDown(0))
        {
            PhotonNetwork.Instantiate(bulletPhotonView.name, spawnBullet.transform.position, spawnBullet.transform.rotation,0);
        }*/
    }
    [PunRPC]
    void Shoot()
    {
        Instantiate(bullet, spawnBullet.transform.position, spawnBullet.transform.rotation);

    }
    public void TakeDamage(float value)
    {
        photonView.RPC("TakeDamageNetwork", RpcTarget.AllBuffered);
    }
    public void TakeDamageNetwork(float value)
    {
        HealthManager(value);
    }
    void HealthManager(float value)
    {
        playerHealtCurrent += value;
        playerHealthFill.fillAmount = playerHealtCurrent/100f;
    }
    void PlayerMove()
    {
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");
        rigidbody2D.velocity = new Vector2(x,y);
    }

    void PlayerTurn()
    {
        Vector3 mousePosition = Input.mousePosition;

        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        transform.up = direction;
    }
}
